package hello

import (
	"github.com/davecgh/go-spew/spew"
	"rsc.io/quote"
	quoteV3 "rsc.io/quote/v3"
)

func Proverb() string {
	return quoteV3.Concurrency()
}

func Hello() string {
	return quote.Hello()
}

func Dumpc() string {
	a := 3
	b := "qq"
	return spew.Sdump(a, b)
}
