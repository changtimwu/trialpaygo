//go:generate go-enum --marshal --noprefix -f=$GOFILE

package hello

// QTID is an enumeration of QFinder tag ID that are allowed.
/* ENUM(
SERVER_NAME                            =     1 // 0x01
MY_VERSION                                =     2 // 0x02
)*/
type QTID int
