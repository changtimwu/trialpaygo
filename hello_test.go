package hello

import "testing"

func TestHello(t *testing.T) {
	want := "Hello, world."
	if got := Hello(); got != want {
		t.Errorf("Hello() = %q, want %q", got, want)
	}
}

func TestDumpc(t *testing.T) {
	want := "(int) 3\n(string) (len=2) \"qq\"\n"
	if got := Dumpc(); got != want {
		t.Errorf("Dumpc() = %q, want %q", got, want)
	}
}
