module gitlab.com/changtimwu/trialpaygo

require (
	github.com/abice/go-enum v0.1.7-0.20190405170835-c5b07ccb2ba8 // indirect
	github.com/davecgh/go-spew v1.1.1
	rsc.io/quote v1.5.2
	rsc.io/quote/v3 v3.1.0
)

go 1.13
